# Go Little M.U.D.

Born from [Go M.U.D.](https://gitlab.com/alienspaces/go-mud). 

All hail Go Little M.U.D.

- A M.U.D. game engine and mobile client
- Simple deployment 
- Low cost

❗ On hold while I develop the concept as a single player game first ❗

