package server

import (
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"

	"alienspaces/go-little-mud/backend/core/utility/configurer"
)

func TestNewServer(t *testing.T) {
	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			name:    "standard server",
			wantErr: false,
		},
	}

	c, err := configurer.NewConfigurer(map[string]string{
		"APP_LOG_LEVEL": zerolog.LevelDebugValue,
	})
	require.NoError(t, err, "NewConfigurer returns without error")

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewServer(c)
			if tt.wantErr {
				require.Error(t, err, "NewServer returns expected error")
			}
			require.NoError(t, err, "NewServer returns without error")
			require.NotNil(t, got, "NewServer returns a server")
		})
	}
}
