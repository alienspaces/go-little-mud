package http

import (
	"alienspaces/go-little-mud/backend/core/utility"
	"context"
)

type HTTP struct {
	config Config
}

func NewHTTP(configurer utility.Configurer) (*HTTP, error) {

	config, err := NewConfig(configurer)
	if err != nil {
		return nil, err
	}

	s := &HTTP{
		config: *config,
	}

	return s, nil
}

func (h *HTTP) Run(ctx context.Context) error {
	return nil
}
