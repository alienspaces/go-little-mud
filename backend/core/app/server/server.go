package server

import (
	"context"

	"github.com/rs/zerolog"

	"alienspaces/go-little-mud/backend/core/app/server/daemon"
	"alienspaces/go-little-mud/backend/core/app/server/http"
	"alienspaces/go-little-mud/backend/core/utility"
	"alienspaces/go-little-mud/backend/core/utility/logger"
)

type Server struct {
	config Config
	log    *zerolog.Logger
	http   *http.HTTP
	daemon *daemon.Daemon
}

func NewServer(configurer utility.Configurer) (*Server, error) {

	config, err := NewConfig(configurer)
	if err != nil {
		return nil, err
	}

	l, err := logger.NewLogger(configurer)
	if err != nil {
		return nil, err
	}

	h, err := http.NewHTTP(configurer)
	if err != nil {
		return nil, err
	}

	d, err := daemon.NewDaemon(configurer)
	if err != nil {
		return nil, err
	}

	s := &Server{
		config: *config,
		log:    l,
		http:   h,
		daemon: d,
	}

	return s, nil
}

func (s *Server) Run() (err error) {

	// TODO: Signal handling

	ctx, cancelFunc := context.WithCancel(context.Background())
	defer cancelFunc()

	err = s.http.Run(ctx)
	if err != nil {
		return err
	}

	err = s.daemon.Run(ctx)
	if err != nil {
		return err
	}

	return nil
}
