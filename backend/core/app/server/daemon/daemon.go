package daemon

import (
	"alienspaces/go-little-mud/backend/core/utility"
	"context"
)

type Daemon struct {
	config Config
}

func NewDaemon(configurer utility.Configurer) (*Daemon, error) {

	config, err := NewConfig(configurer)
	if err != nil {
		return nil, err
	}

	s := &Daemon{
		config: *config,
	}

	return s, nil
}

func (h *Daemon) Run(ctx context.Context) error {
	return nil
}
