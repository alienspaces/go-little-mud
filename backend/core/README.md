# core

Contains packages for building servers that provide:

- JSON APIs over HTTP and Websockets
- Asynchronous job processing
- SQL databases
- Layered architecture with App, Domain and Data layers
