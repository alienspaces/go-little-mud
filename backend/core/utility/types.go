package utility

type Configurer interface {
	Parse(config interface{}) error
}
