package fault

import "fmt"

const (
	CodeInternalError int = 1
)

type Fault struct {
	code    int
	message string
	args    []any
}

func (e Fault) Error() string {
	return fmt.Errorf(e.message, e.args...).Error()
}

func InternalFault(message string, args ...any) error {
	return Fault{code: CodeInternalError, message: message, args: args}
}
