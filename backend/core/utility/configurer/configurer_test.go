package configurer

import (
	"os"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestParse(t *testing.T) {

	os.Setenv("APP_TEST_VALUE_ONE", "1")
	os.Setenv("APP_TEST_VALUE_TWO", "Two")

	type testConfig struct {
		ValueOne   int    `env:"APP_TEST_VALUE_ONE,required"`
		ValueTwo   string `env:"APP_TEST_VALUE_TWO,required"`
		ValueThree int    `env:"APP_TEST_VALUE_THREE,required" envDefault:"3"`
		ValueFour  string `env:"APP_TEST_VALUE_FOUR,required" envDefault:"Four"`
	}

	tests := []struct {
		name    string
		cfg     *testConfig
		wantErr bool
	}{
		{
			name:    "standard configuration",
			cfg:     &testConfig{},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := Parse(tt.cfg)
			if tt.wantErr {
				require.Error(t, err, "Parse returns an error")
				return
			}
			require.NoError(t, err, "Parse returns without error")
			require.NotNil(t, got, "Parse returns a logger")
		})
	}
}
