package configurer

import (
	env "github.com/caarlos0/env/v11"
)

type Configurer struct {
	values map[string]string
}

func NewConfigurer(values map[string]string) (*Configurer, error) {
	return &Configurer{values: values}, nil
}

func (c Configurer) Parse(config interface{}) error {

	err := env.ParseWithOptions(config, env.Options{Environment: c.values})
	if err != nil {
		return nil
	}

	return nil
}

func ParseWithValues[T any](config *T, values map[string]string) (*T, error) {

	err := env.ParseWithOptions(config, env.Options{Environment: values})
	if err != nil {
		return nil, nil
	}

	return config, err
}

func Parse[T any](config *T) (*T, error) {

	if err := env.Parse(config); err != nil {
		return nil, err
	}

	return config, nil
}
