package database

type Database struct {
	config Config
}

func NewDatabase() (*Database, error) {

	return &Database{}, nil
}

func NewDatabaseWithConfig(cfg Config) (*Database, error) {

	d := &Database{
		config: cfg,
	}

	return d, nil
}
