package database

import (
	"alienspaces/go-little-mud/backend/core/utility/configurer"
)

type Config struct{}

func NewConfig() (*Config, error) {
	cfg := &Config{}
	cfg, err := configurer.Parse[Config](cfg)
	if err != nil {
		return nil, err
	}
	return cfg, nil
}
