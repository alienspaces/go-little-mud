package logger

import (
	"testing"

	"github.com/rs/zerolog"
	"github.com/stretchr/testify/require"

	"alienspaces/go-little-mud/backend/core/utility/configurer"
)

func TestNewLogger(t *testing.T) {

	tests := []struct {
		name    string
		wantErr bool
	}{
		{
			name:    "standard logger",
			wantErr: false,
		},
	}

	c, err := configurer.NewConfigurer(map[string]string{
		"APP_LOG_LEVEL": zerolog.LevelDebugValue,
	})
	require.NoError(t, err, "NewConfigurer returns without error")

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := NewLogger(c)
			if tt.wantErr {
				require.Error(t, err, "NewLogger returns an error")
				return
			}
			require.NoError(t, err, "NewLogger returns without error")
			require.NotNil(t, got, "NewLogger returns a logger")
		})
	}
}
