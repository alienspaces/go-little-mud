package logger

import (
	"github.com/rs/zerolog"

	"alienspaces/go-little-mud/backend/core/utility"
	"alienspaces/go-little-mud/backend/core/utility/fault"
)

type Config struct {
	LogLevel zerolog.Level `env:"APP_LOG_LEVEL,required"`
}

func NewConfig(c utility.Configurer) (*Config, error) {

	if c == nil {
		return nil, fault.InternalFault("missing configurer")
	}

	var config Config
	err := c.Parse(&config)
	if err != nil {
		return nil, err
	}
	return &config, nil
}
