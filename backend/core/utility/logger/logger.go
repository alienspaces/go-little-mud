package logger

import (
	"os"

	"github.com/rs/zerolog"

	"alienspaces/go-little-mud/backend/core/utility"
)

func NewLogger(configurer utility.Configurer) (*zerolog.Logger, error) {

	config, err := NewConfig(configurer)
	if err != nil {
		return nil, err
	}

	logger := zerolog.New(os.Stderr).With().Timestamp().Logger()

	zerolog.SetGlobalLevel(config.LogLevel)

	return &logger, nil
}
