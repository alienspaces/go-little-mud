module alienspaces/go-little-mud/backend/app

go 1.22.0

replace alienspaces/go-little-mud/backend/core => ../core

require alienspaces/go-little-mud/backend/core v0.0.0-00010101000000-000000000000

require (
	github.com/caarlos0/env/v11 v11.0.0 // indirect
	github.com/mattn/go-colorable v0.1.13 // indirect
	github.com/mattn/go-isatty v0.0.19 // indirect
	github.com/rs/zerolog v1.32.0 // indirect
	golang.org/x/sys v0.12.0 // indirect
)
