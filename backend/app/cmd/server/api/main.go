package main

import (
	server "alienspaces/go-little-mud/backend/app/internal/server/api"
	"alienspaces/go-little-mud/backend/core/utility/configurer"

	"os"
)

func main() {

	configurer, err := configurer.NewConfigurer(nil)
	if err != nil {
		os.Exit(1)
	}

	server, err := server.NewServer(configurer)
	if err != nil {
		os.Exit(1)
	}

	err = server.Run()
	if err != nil {
		os.Exit(1)
	}

	os.Exit(0)
}
