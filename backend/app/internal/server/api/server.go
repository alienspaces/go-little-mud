package server

import (
	"alienspaces/go-little-mud/backend/core/app/server"
	"alienspaces/go-little-mud/backend/core/utility"
)

func NewServer(c utility.Configurer) (*server.Server, error) {

	s, err := server.NewServer(c)
	if err != nil {
		return nil, err
	}

	return s, nil
}
